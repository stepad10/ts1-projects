package cz.cvut.fel.ts1.ts1.lab02;

/**
 *
 * @author david
 */
public class stepad10 {

    public long factorial(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public long factorial2(int n){
        long ret = n;
        for(int i=1; i<(n-1); i++){
            ret=ret*(n-i);
        }
        return ret;
    }
}
