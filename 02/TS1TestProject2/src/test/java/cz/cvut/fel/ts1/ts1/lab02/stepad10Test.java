package cz.cvut.fel.ts1.ts1.lab02;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 *
 * @author david
 */
public class stepad10Test {

    @Test
    public void factorialTest() {
        stepad10 temp = new stepad10();
        System.out.println(temp.factorial(5));
        Assertions.assertTrue(temp.factorial(5) == 120);
    }
}
